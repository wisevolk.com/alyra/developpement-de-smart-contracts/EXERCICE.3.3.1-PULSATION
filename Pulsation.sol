pragma solidity >=0.5.0 < 0.7.0;

contract Pulsation {

    uint public battement;

    constructor() public {
        battement = 0;
    }

    function ajouterBattement() public {
        battement++;
    }
}
